$(document).ready(function() {

  var state = $('#tip').is(':checked');
  if(state){
    $(".obvezni_oddelki").show(0);
  }else{
    $(".obvezni_oddelki").hide(0);
  }

  $("#tip").change(function() {
    state = $(this).is(':checked');
    if(state){
      $(".obvezni_oddelki").show(500);
    }else{
      $(".obvezni_oddelki").hide(0);
      $(".obvezni_oddelki input[type=checkbox]").removeAttr('checked');
    }
  });

});
