<?php

class Controller{

  public function __construct(){

  }

  public function model($model){

    require_once 'app/models/'.$model.'.php';
    return new $model;

  }

  public function view($view,$data = []){

    // Get years
    $data['leta'] = $this->returnYears();
    //$v = new View($view,$data);
    $v = new View();
    $v->render($view,$data);

  }

  public function returnYears(){

    $years = Model::allYears();

    return $years;

  }

  public function filterTextInput($string){

    return htmlspecialchars(filter_var($string,FILTER_SANITIZE_STRING));

  }

  public function filterIntInput($int){

    return abs(filter_var($int,FILTER_SANITIZE_NUMBER_INT));

  }

}
