<?php

class View{

  public function __construct(){

  }

  public function render($view,$data = []){

    // Static html includes
    include_once 'app/views/page_head.php';

    // If admin include head menu
    include_once 'app/views/main_header.php';

    // Include message boxes
    include_once 'app/views/main_message_boxes.php';

    // Dynamic View
    if($view != "") include 'app/views/'.$view.'.php';

    // Statc html includes
    include_once 'app/views/main_footer.php';

  }

}
