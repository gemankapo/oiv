<h1><?= $data['view_title'] ?></h1>
<form action="<?= $data['form_action'] ?>" method="POST">
  <table>
    <tr>
      <td class="table_label">Naziv</td>
      <td><input type="text" name="naziv" value="<?= (isset($data['naziv']))? $data['naziv'] : "" ?>" /></td></td>
      <td class="table_description"></td>
    </tr>

    <tr>
      <td class="table_label">Opis</td>
      <td><textarea name="opis" cols="50" rows="10"><?= (isset($data['opis']))? $data['opis'] : "" ?></textarea></td>
      <td class="table_description">Podrobnejši opis in informacije o dejavnosti</td>
    </tr>

    <tr>
      <td class="table_label">Ure</td>
      <td><input type="text" name="ure" value="<?= (isset($data['ure']))? $data['ure'] : "" ?>" /></td>
      <td class="table_description">Število opravljenih ur za dejavnost.</td>
    </tr>

    <tr>
      <td class="table_label">Izvajalec</td>
      <td><input type="text" name="izvajalec" value="<?= (isset($data['izvajalec']))? $data['izvajalec'] : "" ?>" /></td>
      <td class="table_description">Ime izvajalca oz. izvajalcev dejavnosti</td>
    </tr>

    <tr>
      <td class="table_label">Kraj</td>
      <td><input type="text" name="kraj" value="<?= (isset($data['kraj']))? $data['kraj'] : "" ?>" /></td>
      <td class="table_description">Kraj izvedbe dejavnosti</td>
    </tr>

    <tr>
      <td class="table_label">Cena</td>
      <td><input type="text" name="cena" value="<?= (isset($data['cena']))? $data['cena'] : "" ?>" /></td>
      <td class="table_description">Cena dejavnosti</td>
    </tr>

    <tr>
      <td class="table_label">Datum</td>
      <td><input type="text" name="datum" value="<?= (isset($data['datum']))? $data['datum'] : "" ?>" /></td>
      <td class="table_description">Datum izvedbe dejavnosti</td>
    </tr>

    <?php if($data["nodijaki"] !== TRUE){ ?>
    <tr>
      <td class="table_label">Dijaki</td>
      <td colspan="2">
        <select id="dejavnosti_prijave" name="dejavnosti_prijave[]" multiple>
        <?php
          $oddelek = "";
          foreach($data['dijaki'] AS $dijak_id => $dijak){
            if($dijak['oddelek'] != $oddelek){
              $oddelek = $dijak['oddelek'];
        ?>
          </optgroup><optgroup label="<?= $oddelek ?>">
        <?php } ?>

          <option value="<?= $dijak_id ?>" <?= (!empty($data['dejavnosti_prijave'][$dijak_id]['selected']))? "selected" : "" ?>><?= $dijak['ime']." ".$dijak['priimek'] ?></option>
        <?php } ?>
        </optgroup>
        <select>
      </td>
    </tr>
  <?php } ?>

    <tr>
      <td class="table_label"><label for="realizirano">Realizirano</label></td>
      <td><input type="checkbox" name="realizirano" id="realizirano" value="1" <?= (isset($data['realizirano']))? ($data['realizirano']=="1")? "checked" : "" : "" ?>/></td>
      <td class="table_description">Ali je bila dejavnost že izvedena</td>
    </tr>

    <tr>
      <td class="table_label"><label for="tip">Dodeljeno</label></td>
      <td><input type="checkbox" name="tip" value="1" id="tip" <?= (isset($data['tip']))? ($data['tip']=="1")? "checked" : "" : ""?>/></td>
      <td class="table_description">Ali je bila dejavnost dodeljena s strani šole oz. samostojno izbrana</td>
    </tr>

    <tr class="obvezni_oddelki">
      <td colspan="2">
    <?php $temp_letnik = 1; foreach($data['vsi_oddelki'] as $letnik => $razredi){
      if($letnik != $temp_letnik){ $temp_letnik = $letnik; echo "<br />"; }
      foreach($razredi as $razred){
    ?>
        <input type="checkbox" name="oddelki[<?= $razred ?>]" value="<?= $razred ?>" id="<?= $razred ?>" <?= (isset($data['oddelki'][$razred]))? "checked" : "" ?>/><label for="<?= $razred ?>"><?= $razred ?></label>
    <?php } } ?>
      <td class="table_description">Oddelki za katere je prisotnost obvezna.</td>
    </tr>


    <tr>
      <td class="table_label">Kategorija</td>
      <td>
        <select name="kategorija">
          <?php foreach($data['kategorije'] as $kategorije): ?>
            <option value="<?= $kategorije['id'] ?>" <?php if(isset($data['kategorija'])){ if($data['kategorija'] == $kategorije['id']){ echo "selected"; }} ?>><?= $kategorije['naziv'] ?></option>
          <?php endforeach; ?>
        </select>
      </td>
      <td class="table_description"></td>
    </tr>

  </table>
  <?php require_once "app/views/submit_buttons.php"; ?>
</form>
