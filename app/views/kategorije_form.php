<h1><?= $data['view_title'] ?></h1>
<form action="<?= $data['form_action'] ?>" method="POST">
  <table>
    <tr>
      <td class="table_label">Naziv kategorije</td>
      <td><input type="text" name="naziv" value="<?= (isset($data['naziv']))? $data['naziv'] : "" ?>"></td>
    </tr>
  </table>
  <?php require_once "app/views/submit_buttons.php"; ?>
</form>
