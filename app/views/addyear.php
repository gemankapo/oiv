<h1><?= $data['view_title'] ?></h1>

<?php if($data['showform'] === TRUE){ ?>

  <form action="<?= $data['form_action'] ?>" method="post" enctype="multipart/form-data">
    <table>
      <tr><td class="table_label">Leto</td><td><input type="text" name="yearid" value="<?= (isset($data['yearid']))? $data['yearid'] : "" ?>"></td><td class="table_description">Številka, primer: 2017</td></tr>
      <tr><td class="table_label">Naziv</td><td><input type="text" name="yearname" value="<?= (isset($data['yearname']))? $data['yearname'] : "" ?>"></td><td class="table_description">Naziv šolskega leta, primer: 2017/18</td></tr>
        <tr><td class="table_label">Dijaki</td><td><input type="file" name="yearfile" id="yearfile"></td><td class="table_description">.csv datoteka z vsemi vpisanimi dijaki izbranega leta iz sistema eAssistent. Datoteka mora imeti 4 stolpce; ID učenca, Priimek, Ime, Oddelek. Primer vrstice:<br />8462337,Zver,Miha,1.A</td></tr>
    </table>
    <input class="submit_buttons" id="submit_save" type="submit" name="submit" value="Ustvari">
  </form>

<?php } ?>
