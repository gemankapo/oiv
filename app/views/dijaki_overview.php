<h1><?= $data['ime']." ".$data['priimek'] ?></h1>
<h3>Opravljene dejavnosti</h2>
<?php if(isset($data['dejavnosti'])){ ?>
<table class="table_border">
  <tr><th>Naziv</th><th>Ure</th><th>Kategorija</th></tr>
<?php foreach($data['dejavnosti'] as $key => $dejavnost){ ?>
  <tr><td><?= $dejavnost['naziv'] ?></td><td><?= $dejavnost['ure'] ?></td><td><?= $dejavnost['kategorija'] ?></td></tr>
<?php } ?>
</table>
  <hr />
  <p>Dodeljene ure: <?= $data['tip_1'] ?></p>
  <p>Izbirne ure: <?= $data['tip_0'] ?></p>
  <p>Skupaj ur: <?= $data['tip_0'] + $data['tip_1'] ?></p>
<?php } ?>
