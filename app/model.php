<?php

class Model{

  public function __construct(){

  }

  public function connect(){

    // create mysql connection
    $connection = new mysqli(DB_HOST,DB_USER,DB_PWD,DB_NAME);

    // change character set to utf8
    $connection->set_charset("utf8");

    // check connection
    if($connection->connect_error)
      trigger_error('Database connection failed: '.$connection->connect_error,E_USER_ERROR);
    return $connection;

  }

  public static function yearCurrent(){

    $connection = (new Model)->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT max(leto) FROM leto");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Execute statement
    $stmt->execute();

    // bind result
    $stmt->bind_result($year);
    while($stmt->fetch()){
      return $year;
    }

    // Clear memory
    $stmt->close();

  }

  public static function yearExists($year){

    $connection = (new Model)->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT leto FROM leto WHERE leto=?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$year);

    // Execute statement
    $stmt->execute();

    // Store results so num_rows can work
    $stmt->store_result();

    // get number of rows
    return ($stmt->num_rows === 1)? true : false;

    // Clear memory
    $stmt->close();

  }

  public static function allYears(){

    $connection = (new Model)->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT leto,naziv FROM leto");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($leto,$naziv);

    // create data array
    $data = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($data,["leto" => $leto,"naziv" => $naziv]);
    }

    // return data
    return $data;

    // Clear memory
    $stmt->close();

  }

}
