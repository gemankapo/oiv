<?php

class m_oddelki extends Model{

  public function index(){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT naziv,letnik FROM oddelek");

    // Return any errors
    if($stmt === false){
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($oddelek,$letnik);

    // create data array
    $data = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($data,["letnik" => $letnik,"oddelek" => $oddelek]);
    }

    // return data
    return $data;

    // Clear memory
    $stmt->close();

  }

}
