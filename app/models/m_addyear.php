<?php

class m_addyear extends Model{

  public function CheckExistDijak($id){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT id FROM dijak WHERE id = ?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$id);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($existsId);

    while ($stmt->fetch()) {
      $return = $existsId;
    }

    if(isset($return)){
        return TRUE; // exists
    }else{
      return FALSE; // doesn't exist
    }

    // Clear memory
    $stmt->close();

  }

  public function insertDijak($dijak){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO dijak (id,ime,priimek) VALUES (?,?,?)");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('iss',$dijak[0],$dijak[2],$dijak[1]);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

  public function addDijakTOOddelek($dijak,$oddelek,$leto){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO dijak_oddelek (dijak_id,oddelek,leto) VALUES (?,?,?)");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('isi',$dijak,$oddelek,$leto);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

  public function addYear($yearid,$yearname){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO leto (leto,naziv) VALUES (?,?)");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('is',$yearid,$yearname);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

}
