<?php

class m_dijaki extends Model{

  public function index($leto){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dijak.id,dijak.ime,dijak.priimek,dijak_oddelek.oddelek FROM dijak INNER JOIN dijak_oddelek ON (dijak.id = dijak_oddelek.dijak_id AND dijak_oddelek.leto = ?) ORDER BY dijak_oddelek.oddelek ASC,dijak.priimek ASC,dijak.ime ASC;");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$leto);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($id,$ime,$priimek,$oddelek);

    // create data array
    $data = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($data,["oddelek" => $oddelek,"id" => $id,"ime" => $ime,"priimek" => $priimek]);
    }

    // return data
    return $data;

    // Clear memory
    $stmt->close();

  }

  public function returnDijak(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dijak.ime,dijak.priimek,dijak_oddelek.oddelek FROM dijak INNER JOIN dijak_oddelek ON (dijak.id = dijak_oddelek.dijak_id) WHERE dijak.id = ? AND dijak_oddelek.leto = ?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('ii',$data['id'],$_SESSION['leto']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($ime,$priimek,$oddelek);


    // push data into array
    while ($stmt->fetch()) {
      $data['ime'] = $ime;
      $data['priimek'] = $priimek;
      $data['oddelek'] = $oddelek;
    }

    // Clear memory
    $stmt->close();

  }

  public function opravljeneDejavnosti(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT dejavnost.id,dejavnost.naziv,dejavnost.ure,dejavnost.tip,kategorija.naziv FROM dijak_dejavnost INNER JOIN dejavnost ON (dijak_dejavnost.dejavnost_id = dejavnost.id) INNER JOIN kategorija_leto ON (dejavnost.kategorija_id = kategorija_leto.kategorija_id) INNER JOIN kategorija ON (kategorija_leto.kategorija_id = kategorija.id)
    WHERE dijak_dejavnost.dijak_id = ? AND kategorija_leto.leto = ? AND dejavnost.realizirano IN (1,0) ORDER BY dejavnost.tip DESC, kategorija.naziv ASC, dejavnost.id DESC");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('ii',$data['id'],$_SESSION['leto']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($d_id,$d_naziv,$d_ure,$d_tip,$k_naziv);

    $data['tip_0'] = 0;
    $data['tip_1'] = 0;

    // push data into array
    while ($stmt->fetch()) {
      $data['dejavnosti'][$d_id]['naziv'] = $d_naziv;
      $data['dejavnosti'][$d_id]['ure'] = $d_ure;
      $data['dejavnosti'][$d_id]['tip'] = $d_tip;
      $data['dejavnosti'][$d_id]['kategorija'] = $k_naziv;
      $data['tip_'.$d_tip] += $d_ure;
    }

    // Clear memory
    $stmt->close();

  }

  public function returnOddelki(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT naziv FROM oddelek");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($oddelek);


    // push data into array
    while ($stmt->fetch()) {
      $data['oddelki'][$oddelek] = $oddelek;
    }

    // Clear memory
    $stmt->close();

  }

  public function addDijak(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO dijak (id,ime,priimek) VALUES (?,?,?)");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('iss',$data['id'],$data['ime'],$data['priimek']);

    // Execute statement
    $stmt->execute();

    $result = $stmt->affected_rows;

    $data['id'] = $stmt->insert_id;

    if($result > 0){

      $connection = $this->connect();

      // Prepare statement
      $stmt = $connection->prepare("INSERT INTO dijak_oddelek (dijak_id,oddelek,leto) VALUES (?,?,?)");

      // Return any errors
      if($stmt === false) {
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
      }

      // Bind parameters; s = string, i = integer, d = double,  b = blob
      $stmt->bind_param('isi',$data['id'],$data['oddelek'],$_SESSION['leto']);

      // Execute statement
      $stmt->execute();

    }

    // Clear memory
    $stmt->close();

    return $result;

  }

  public function changeDijak(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("UPDATE dijak SET id=?,ime=?,priimek=? WHERE id = ?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('issi',$data['id'],$data['ime'],$data['priimek'],$data['old_id']);

    // Execute statement
    $stmt->execute();

    $result = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    $stmt = $connection->prepare("UPDATE dijak_oddelek SET oddelek=? WHERE dijak_id = ?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('si',$data['oddelek'],$data['old_id']);

    // Execute statement
    $stmt->execute();

    if($result == 0)
      $result = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    return $result;

  }

  public function deleteDijak($id){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("DELETE FROM dijak WHERE id = ?");

    // Return any errors
    if($stmt === false) {
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$id);

    // Execute statement
    $stmt->execute();

    $result = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    return $result;

  }

}
