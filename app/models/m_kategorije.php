<?php

class m_kategorije extends Model{

  public function index($year){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT kategorija.id,kategorija.naziv FROM kategorija INNER JOIN kategorija_leto ON (kategorija.id = kategorija_leto.kategorija_id AND kategorija_leto.leto = ?)");

    // Return any errors
    if($stmt === false){
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$year);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($id,$naziv);

    // create data array
    $data = [];

    // push data into array
    while ($stmt->fetch()) {
      array_push($data,["id" => $id,"naziv" => $naziv]);
    }

    // Clear memory
    $stmt->close();

    // return data
    return $data;

  }

  public function returnKategorija(&$data){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("SELECT naziv FROM kategorija WHERE id = ?");

    // Return any errors
    if($stmt === false){
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$data['id']);

    // Execute statement
    $stmt->execute();

    // Put result into variables
    $stmt->bind_result($naziv);

    // push data into array
    while ($stmt->fetch()) {
      $data['naziv'] = $naziv;
    }

    // Clear memory
    $stmt->close();

  }

  public function changeKategorija($id,$naziv){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("UPDATE kategorija SET naziv = ? WHERE id = ?");

    // Return any errors
    if($stmt === false){
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('si',$naziv,$id);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

  public function deleteKategorija($id){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("DELETE FROM kategorija WHERE id=?");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('i',$id);

    // Execute statement
    $stmt->execute();

    // Set affected rows
    $return = $stmt->affected_rows;

    // Clear memory
    $stmt->close();

    // return affected rows
    return $return;

  }

  public function addKategorija($naziv,$leto){

    $connection = $this->connect();

    // Prepare statement
    $stmt = $connection->prepare("INSERT INTO kategorija (naziv) VALUES (?)");

    // Return any errors
    if($stmt === false){
      trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
    }

    // Bind parameters; s = string, i = integer, d = double,  b = blob
    $stmt->bind_param('s',$naziv);

    // Execute statement
    $stmt->execute();

    $result1 = $stmt->affected_rows;
    $result2 = 0;

    $id = $stmt->insert_id;

    // Clear memory
    $stmt->close();

    if($result1 > 0){

      // Prepare statement
      $stmt = $connection->prepare("INSERT INTO kategorija_leto (kategorija_id,leto) VALUES (?,?)");

      // Return any errors
      if($stmt === false){
        trigger_error("SQL error: " . $connection->error, E_USER_ERROR);
      }

      // Bind parameters; s = string, i = integer, d = double,  b = blob
      $stmt->bind_param('is',$id,$leto);

      // Execute statement
      $stmt->execute();

      // Set affected rows
      $result2 = $stmt->affected_rows;

      // Clear memory
      $stmt->close();

    }

    if(($result1 > 0) AND ($result1 != $result2))
      $this->deleteKategorija($id);

    return $result2;

  }

}
