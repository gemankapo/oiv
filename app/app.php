<?php

class App{

  protected $controller = "home";
  protected $method = "index";
  protected $params = [];

  public function __construct(){

    // Clean and explode URL
    $url = $this->parseUrl();

    // Set a new year session or change current year session
    $this->yearSet();

    // Check if user has access (is logged in)
    if($this->loggedUser() || $url[0] === "prijava"){

      // Check and include controller
      if(file_exists('app/controllers/'. $url[0] .'.php')){
        $this->controller = $url[0];
        unset($url[0]);
      }

    }

    // Include controller and set global controller variable
    require_once 'controllers/'. $this->controller .'.php';

    $GLOBALS['controller'] = $this->controller;

    // Call this Controller non-statically
    $this->controller = new $this->controller;

    // Check method
    if(isset($url[1])){
      if(method_exists($this->controller, $url[1])){
        $this->method = $url[1];
        unset($url[1]);
      }
    }

    // Set parameters
    $this->params = $url ? array_values($url) : [];

    // Call Controller, Method and send parameters
    call_user_func_array([$this->controller,$this->method],$this->params);

  }

  public function parseUrl(){

    // if GET is set -> R trim "/" -> sanitize url -> explode by "/" -> return array
    if(isset($_GET['url'])) return $url = explode('/',filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));

  }

  public static function loggedUser(){

    // If user is logged in return TRUE (or FALSE)
    return (isset($_SESSION['user'])) ? true : false;

  }

  public function yearSet(){

    // Check if user changed year
    if(isset($_POST['izbrano_leto'])){
      // Check if it's number
      if(is_numeric($_POST['izbrano_leto'])){
        // Check if year exists in DB
        if(Model::yearExists($_POST['izbrano_leto']) === true){
          // set year session to selected year
          $_SESSION['leto'] = $_POST['izbrano_leto'];
        }
      }
    }

    // If year session isn't set find last year in DB and set it
    if(!isset($_SESSION['leto'])) $_SESSION['leto'] = Model::yearCurrent();

  }

}
