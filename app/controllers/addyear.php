<?php

class addyear extends Controller{

  public function index(){

    $model = $this->model('m_'.get_class($this));

    $data['view_title'] = "Dodaj Leto";
    $data['form_action'] = URL.__CLASS__;
    $data['showform'] = TRUE;

    if(isset($_POST['submit'])){

      $data['showform'] = FALSE;
      $data['yearid'] = $_POST['yearid'];
      $data['yearname'] = $_POST['yearname'];

      $tmpName = $_FILES['yearfile']['tmp_name'];
      $dijaki = array_map('str_getcsv', file($tmpName));


      if($model->addYear($data['yearid'],$data['yearname']) > 0){
        $data['success_msg'] = "Leto uspešno dodano!";

        foreach($dijaki AS $dijak){

          if($model->CheckExistDijak($dijak[0]) === FALSE){

            $model->insertDijak($dijak);

          }

          $model->addDijakToOddelek($dijak[0],$dijak[3],$data['yearid']);

        }

      }else{
        $data['error_msg'] = "Leto neuspešno dodano!";
      }

    }

    $view = $this->view(get_class($this),$data);

    }

}
