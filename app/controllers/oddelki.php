<?php

class oddelki extends Controller{

  public function index(){

    $model = $this->model('m_'.get_class($this));
    $data['oddelki'] = $model->index();

    $data['view_title'] = "Oddelki";
    $this->view(get_class($this),$data);

  }

}
