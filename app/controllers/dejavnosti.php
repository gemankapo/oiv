<?php

class dejavnosti extends Controller{

  public function index(){

    $model = $this->model('m_'.get_class($this));
    $data['dejavnosti'] = $model->index($_SESSION['leto']);

    $data['view_title'] = "Dejavnosti";
    $this->view(get_class($this),$data);

  }

  public function change($id){

    $data['id'] = $this->filterIntInput($id);
    $data['nodijaki'] = FALSE; // to prevent php notice (false = show dropdown to pick dijaki

    $model = $this->model('m_'.get_class($this));
    $model->returnDejavnost($data);

    if(isset($data['leto'])){

      $data['kategorije'] = $model->returnKategorije($data['leto']);

      if(isset($_POST['submit'])){

        $data['naziv'] = $this->filterTextInput($_POST['naziv']);
        $data['opis'] = $this->filterTextInput($_POST['opis']);
        $data['ure'] = $this->filterIntInput($_POST['ure']);
        $data['izvajalec'] = $this->filterTextInput($_POST['izvajalec']);
        $data['kraj'] = $this->filterTextInput($_POST['kraj']);
        $data['cena'] = $this->filterTextInput($_POST['cena']);
        $data['datum'] = $this->filterTextInput($_POST['datum']);
        $data['dejavnosti_prijave'] = (isset($_POST['dejavnosti_prijave']))? $_POST['dejavnosti_prijave'] : false;
        $data['realizirano'] = (isset($_POST['realizirano']))? $this->filterIntInput($_POST['realizirano']) : "0";
        $data['tip'] = (isset($_POST['tip']))? $this->filterIntInput($_POST['tip']) : 0;
        if(isset($_POST['oddelki']))
          $data['oddelki'] = $_POST['oddelki'];
        $data['kategorija'] = $this->filterIntInput($_POST['kategorija']);

        if(is_numeric($_POST['ure'])){

          if($model->changeDejavnost($data) > 0)
            $data['success_msg'] = "Dejavnost uspešno spremenjena!";
          else
            $data['error_msg'] = "Dejavnost neuspešno spremenjena!";

        }else{

          $data['error_msg'] = "Neveljaven zapis ure!";

        }

      }else{

        $model->returnOddelkiObvezno($data);

      }

      $data['controller'] = __CLASS__;
      $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$data['id'];

      $data['vsi_oddelki'] = $model->returnOddelki($_SESSION['leto']);
      $data['dijaki'] = $model->returnDijaki();
      $model->returnDejavnostiPrijave($data);

      $data['view_title'] = "Urejanje dejavnosti";
      $this->view(get_class($this).'_form',$data);

    }else{

      header('Location: '.URL.__CLASS__);

    }

  }

  public function delete($id){

    $data['id'] = $this->filterIntInput($id);

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['delete'])){

      if($model->deleteDejavnost($data['id'],$_SESSION['leto']) > 0)
        $data['success_msg'] = "Dejavnost uspešno izbrisana!";
      else
        $data['error_msg'] = "Dejavnost neuspešno izbrisana!";

      $this->view("",$data);

    }else{

    $model->returnDejavnost($data);

    $data['preklici'] = URL.__CLASS__."/change/".$data['id'];
    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$data['id'];
    $data['warning_msg'] = "Potrebna potrditev izbrisa!";

    $data['view_title'] = "Izbris dejavnosti";
    $this->view(get_class($this).'_delete',$data);

    }

  }

  public function add(){

    $model = $this->model('m_'.get_class($this));

    $data['kategorije'] = $model->returnKategorije($_SESSION['leto']);
    $data['vsi_oddelki'] = $model->returnOddelki($_SESSION['leto']);
    $data['nodijaki'] = TRUE; // so it doesn't show dijaki dropdown on dejavnost/add

    $data['controller'] = __CLASS__;
    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__;

    $data['view_title'] = "Nova dejavnost";

    if(isset($_POST['submit'])){

      $data['naziv'] = $this->filterTextInput($_POST['naziv']);
      $data['opis'] = $this->filterTextInput($_POST['opis']);
      $data['ure'] = $this->filterIntInput($_POST['ure']);
      $data['izvajalec'] = $this->filterTextInput($_POST['izvajalec']);
      $data['kraj'] = $this->filterTextInput($_POST['kraj']);
      $data['cena'] = $this->filterTextInput($_POST['cena']);
      $data['datum'] = $this->filterTextInput($_POST['datum']);
      $data['realizirano'] = (isset($_POST['realizirano']))? $this->filterIntInput($_POST['realizirano']) : 0;
      $data['tip'] = (isset($_POST['tip']))? $this->filterIntInput($_POST['tip']) : 0;
      if(isset($_POST['oddelki']))
        $data['oddelki'] = $_POST['oddelki'];
      $data['kategorija'] = $this->filterIntInput($_POST['kategorija']);
      $data['leto'] = $_SESSION['leto'];


      if(is_numeric($_POST['ure'])){

        if($model->addDejavnost($data) > 0)
          $data['success_msg'] = "Dejavnost uspešno dodana!";
        else
          $data['error_msg'] = "Dejavnost neuspešno dodana!";

        $this->view("",$data);

      }else{

        $data['error_msg'] = "Neveljaven zapis ure!";

        $this->view(get_class($this).'_form',$data);

      }

    }else{

      $this->view(get_class($this).'_form',$data);

    }

  }

}
