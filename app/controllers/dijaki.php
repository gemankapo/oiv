<?php

class dijaki extends Controller{

  public function index(){

    $model = $this->model('m_'.get_class($this));
    $data['dijaki'] = $model->index($_SESSION['leto']);

    $data['view_title'] = "Dijaki";
    $view = $this->view(get_class($this),$data);

  }

  public function overview($id){

    $data['id'] = $this->filterIntInput($id);

    $model = $this->model('m_'.get_class($this));

    $model->returnDijak($data);
    $model->opravljeneDejavnosti($data);

    $data['view_title'] = "Pregled dijaka";
    $this->view(get_class($this).'_overview',$data);

  }

  public function add(){

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['submit'])){

      $data['id'] = $this->filterIntInput($_POST['id']);
      $data['ime'] = $this->filterTextInput($_POST['ime']);
      $data['priimek'] = $this->filterTextInput($_POST['priimek']);
      $data['oddelek'] = $this->filterTextInput($_POST['oddelek']);

      if($model->addDijak($data) > 0)
        $data['success_msg'] = "Dijak uspešno dodan!";
      else
        $data['error_msg'] = "Dijak neuspešno dodan!";

    }

    $model->returnOddelki($data);

    $data['view_title'] = "Dodaj dijaka";
    $data['controller'] = __CLASS__;
    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__;

    $this->view(get_class($this).'_form',$data);

  }

  public function change($id){

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['submit'])){

      $data['id'] = $this->filterIntInput($_POST['id']);
      $data['ime'] = $this->filterTextInput($_POST['ime']);
      $data['priimek'] = $this->filterTextInput($_POST['priimek']);
      $data['oddelek'] = $this->filterTextInput($_POST['oddelek']);
      $data['old_id'] = $this->filterTextInput($id);

      if($model->changeDijak($data) > 0)
        $data['success_msg'] = "Dijak uspešno urejen!"; // preveri change
      else
        $data['error_msg'] = "Dijak neuspešno urejen!";

    }else{

      $data['id'] = $this->filterIntInput($id);

      $model->ReturnDijak($data);

    }

    $model->returnOddelki($data);

    $data['view_title'] = "Uredi dijaka";
    $data['controller'] = __CLASS__;
    $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$id;

    $this->view(get_class($this).'_form',$data);

  }

  public function delete($id){

    $data['id'] = $this->filterIntInput($id);

    $model = $this->model('m_'.get_class($this));

    if(isset($_POST['delete'])){

      if($model->deleteDijak($data['id']) > 0)
        $data['success_msg'] = "Dijak uspešno izbrisana!";
      else
        $data['error_msg'] = "Dijak neuspešno izbrisana!";

      $this->view("",$data);

    }else{

      $model->returnDijak($data);

      $data['preklici'] = URL.__CLASS__."/change/".$data['id'];
      $data['form_action'] = URL.__CLASS__."/".__FUNCTION__."/".$data['id'];
      $data['warning_msg'] = "Potrebna potrditev izbrisa!";

      $data['view_title'] = "Izbris dijaka";
      $this->view(get_class($this).'_delete',$data);

    }

  }

}
