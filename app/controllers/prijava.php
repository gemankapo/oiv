<?php

class prijava extends Controller{

  public function index(){

    $data = [];

    if(isset($_POST['user_login'])){

      // sanitize inputs
      $email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
      $pwd = filter_var($_POST['pwd'],FILTER_SANITIZE_STRING);

      // Check if both email and password are given
      if($email != "" && $pwd != ""){

        // Fetch data
        $model = $this->model('m_'.get_class($this));
        $result = $model->adminLogin($email);

        // Check if user exists
        if(!empty($result)){
          // Check passwords
          if(password_verify($pwd,$result['pwd'])){

            $_SESSION['user'] = $result['name'];
            header('Location: '.URL);
            exit;

          }else{
            $data['error_msg'] = "Avtorizacija ni uspela!";
          }
        }else{
          $data['error_msg'] = "Avtorizacija ni uspela!";
        }
      }else{
        $data['error_msg'] = "Manjkajoči podatki!";
      }
    }

    $data['view_title'] = "Prijava";
    $view = $this->view(get_class($this),$data);

  }

}
